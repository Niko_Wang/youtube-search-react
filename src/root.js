import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Search from './search.js';
import VideoList from './video_list.js';
import VideoDetail from './video_detail.js';

export default class Root extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = { 
            videos: [],
            handler: function(search) {
                //this.setState({searchinput: event.target.value});
                fetch('http://192.168.6.119:8000/' + search)
                .then(results => {
                    //console.log(results.json());
                    return results.json();
                }).then(data => {
                    this.setState({videos: data.items});
                    
                });

            }
         };
         this.handler = this.state.handler.bind(this);
    }

    /*componentWillMount() {
        fetch('http://130.216.66.160:8000/PC')
        .then(results => {
            //console.log(results.json());
            return results.json();
        }).then(data => {
            this.setState({videos: data.items});
            
        });
    }*/

    render() {
        if (!this.state.videos) return <p>Loading...</p>
        return(
            <div>
                <Search handler={this.handler} />
                <VideoDetail videos={this.state.videos} />
                <VideoList videos={this.state.videos} />
            </div>
        )
    }
}