import React from 'react';

const VideoDetail = ({videos}) => {
    if (videos[0]) {
        const videoId = videos[0].id.videoId;
        const url = 'https://www.youtube.com/embed/' + videoId;

        return (
            <div className="video-detail col-md-8">
                <div className="embed-responsive embed-responsive-16by9">
                    <iframe className="embed-responsive-item" src={url} />
                </div>
                <div className="details">
                    <div>{videos[0].snippet.title}</div>
                    <div>{videos[0].snippet.description}</div>
                </div>
            </div>
        )

    }

    return <div></div>

}

export default VideoDetail