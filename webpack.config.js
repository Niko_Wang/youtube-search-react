var path = require("path");
var webpack = require('webpack');

module.exports = {
    //context: __dirname + "/src",
  
    /*entry: [
      'react-hot-loader/patch', // RHL patch
      './main.js' // Your appʼs entry point
    ],*/
    entry: {
      app: [
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://0.0.0.0:3000',
        'webpack/hot/only-dev-server',
        './src/index.js'
      ]
    },
  
    /*output: {
      filename: "bundle.js",
      path: __dirname + "public",
    },*/

    output: {
      path: path.resolve(__dirname, "public"),
      publicPath: "/",
      filename: "bundle.js"
    },

    resolve: {
        extensions: ['.js', '.jsx', '.json']
      },
      module: {
        loaders: [
          {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            //loaders: ["babel-loader"]
            loaders: ['react-hot-loader/webpack', 'babel-loader']
          }
        ]
      },
      plugins: [
        new webpack.HotModuleReplacementPlugin()
      ],
      devServer: {
        proxy: {
          '/all': {
            target: 'http://192.168.6.102:3000',
            secure: false
          }
        }
      }
  };